var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var dataURL = false;
var canvas_height = canvas.height;
var canvas_width = canvas.width;
var x = 0;
var y = 0;
var prev_x = 0;
var prev_y = 0;
var decision=false;
var end=false;
var todo = "pencil";
var color_or_type="black";
var color="#000000";
var word=1;
var start_x=0;
var start_y=0;
var image_data1=ctx.getImageData(0,0,canvas.height,canvas.width);
var image_data2=ctx.getImageData(0,0,canvas.height,canvas.width);
var img=0;
var texts=0;
var size=1;
var type=1;
var ip1=10;
var ip2=" Arial";
var music=0;
var download_img;
var flag=false;

ctx.fillStyle="#FFFFFF";
ctx.fillRect(0,0,canvas.width,canvas.height);


music=document.getElementById("music");
music.play();

var cur=document.getElementById("curtype");

cur.style.cursor=" default";

function choose_size(a){
      if(a==10||a==40){
        ip1=a; 
        ip1=ip1.toString();
      }
      else{
        if(ip2=='a') ip2=" Arial"
        else ip2=" Dancing Script"
      }
      console.log(ctx.font);
      ctx.font = ip1+"px"+ip2;
}

var upload=document.querySelector('.upload');upload.addEventListener('change',function(e){
  //console.log(1);
  let reader=new FileReader();
  reader.onload=function(event)
  {
    img=new Image();
    img.onload=function()
    {
      img.height=10;
      img.width=10;
    };
    img.src=this.result;
  };
  reader.readAsDataURL(this.files[0]);
});

function text() {
    texts=prompt("Please Enter The Word","Hello world!");
    if(texts)
    {
      alert('When you click on canvas,the text you enter will show up');
      cur.style.cursor=" n-resize";
    }
    
  }


function colorboard(good){
    color_or_type=good;
    which_color();
}

function which_color(){
  if(color_or_type=="red"){
      color="#ff0000";
  }
  else if(color_or_type=="orange"){
      color="#ff8000";
  }
  else if(color_or_type=="yellow"){
      color="#ffff00";
  }
  else if(color_or_type=="green"){
      color="#00ff00";
  }
  else if(color_or_type=="blue"){
      color="#0000ff";
  }
  else if(color_or_type=="purple"){
      color="#8000ff";
  }
  else if(color_or_type=="black"){
      color="#000000";
  }
  else if(color_or_type=="bold"){
      word=10;
  }
  else if(color_or_type=="slim"){
      word=1;
  }
}

function todo_list(input) {
  todo = input;
  if(todo=="redo"){
    redo();
  }
  else if(todo=="undo"){
    undo();
  }
  else if(todo=="text"){
    text();
  }
  else if(todo=="rectangle"){
    cur.style.cursor=" all-scroll";
  }
  else if(todo=="pencil"||todo=="eraser"){
    cur.style.cursor=" default";
  }
  else if(todo=="circle"){
    cur.style.cursor=" all-scroll";
  }
  else if(todo=="triangle"){
    cur.style.cursor=" all-scroll";
  }
}

function redo(){
  ctx.putImageData(image_data1,0,0)
}

function undo(){
  ctx.putImageData(image_data2,0,0)
}

function initial() {
  canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');
  canvas.addEventListener("mousedown", function (evt) {
    down(evt);
  });
  canvas.addEventListener("mouseup", function (evt) {
    up(evt);
  });
  canvas.addEventListener("mouseout", function (evt) {
    out(evt);
  });
  canvas.addEventListener("mousemove", function (evt) {
    move(evt);
  });
}

function up(evt) {
  image_data2=ctx.getImageData(0,0,canvas.width,canvas.height);
  console.log(image_data2);
  end=true;
  decision = false;
}

function down(evt) {
  prev_x = x;
  prev_y = y;
  start_x=x;
  start_y=y;
  x = evt.clientX - canvas.offsetLeft;
  y = evt.clientY - canvas.offsetTop;
  image_data1=ctx.getImageData(0,0,canvas.width,canvas.height);
  decision = true;
  if (todo == "pencil") {
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.fillRect(x,y,1,1);
    ctx.closePath();
  }
  else if(todo=="draw"){
    save();
  }
  else if(todo=="rectangle"){
    save();
  }
  else if(todo=="circle"){
    save();
  }
  else if(todo=="triangle"){
    save();
  }
  else if(todo=="text"){
    draw_text();
  }
}
function out(evt) {
  end=false;
  decision = false;
}

function move(evt) {
  prev_x = x;
  prev_y = y;
  x = evt.clientX - canvas.offsetLeft;
  y = evt.clientY - canvas.offsetTop;

  if (todo == "pencil") {
    pencil_draw();
  }
  else if(todo=="rectangle"){
    rec_draw();
  }
  else if(todo=="eraser"){
    eraser_draw();
  }
  else if(todo=="circle"){
    cir_draw();
  }
  else if(todo=="triangle"){
    tri_draw();
  }
  else if(todo=="draw"){
    draw();
  }
}

function draw(){
  console.log(todo);
  if(decision==true){
    load();
    console.log("fuck");
    ctx.beginPath();
    ctx.drawImage(img,start_x,start_y,x-start_x,y-start_y);
    ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.drawImage(img,start_x,start_y,x-start_x,y-start_y);
    end=false;
    ctx.closePath();
  }
}

function draw_text(){
    ctx.fillText(texts,x,y);
}

function tri_draw(){
  if(decision==true){
    load();
    ctx.fillStyle=color;
    ctx.beginPath();
    ctx.moveTo(start_x,start_y);
    ctx.lineTo(x,y);
    ctx.lineTo(x-100,y-50);
    ctx.fill();
    ctx.closePath();
  }
  else if(end==true){
    ctx.fillStyle=color;
    ctx.beginPath();
    ctx.moveTo(start_x,start_y);
    ctx.lineTo(x,y);
    ctx.lineTo(x-100,y-50);
    end=false;
    ctx.fill();
    ctx.closePath();
    image_data2=ctx.getImageData(0,0,canvas.width,canvas.height);
  }
}
function cir_draw() {
  draw_or_not=true;
  if(decision==true){
    load();
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.arc(start_x,start_y,Math.abs(x-start_x),0,2*Math.PI);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.arc(start_x,start_y,Math.abs(x-start_x),0,2*Math.PI,true);
    ctx.stroke();
    end=false;
    ctx.fill();
    ctx.closePath();
    image_data2=ctx.getImageData(0,0,canvas.width,canvas.height);
  }
  
}

function rec_draw() {
  if(decision==true){
  load();
  ctx.beginPath();
  ctx.fillStyle=color;
  ctx.fillRect(start_x,start_y,x-start_x,y-start_y);
  ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.fillRect(start_x,start_y,x-start_x,y-start_y);
    end=false;
    ctx.closePath();
    image_data2=ctx.getImageData(0,0,canvas.width,canvas.height);
  }
}

function pencil_draw() {
  if (decision == true) {
    ctx.beginPath();
    ctx.moveTo(prev_x, prev_y);
    ctx.lineTo(x, y);
    ctx.strokeStyle = color;
    ctx.lineWidth = word;
    ctx.stroke();
    ctx.closePath();
  }
}

function eraser_draw() {
  if(decision==true){
    ctx.beginPath();
    ctx.moveTo(prev_x, prev_y);
    ctx.lineTo(x, y); 
    ctx.strokeStyle="#FFFFFF";
    ctx.lineWidth = 10;
    ctx.stroke();
    ctx.closePath();
  }
}

function clean_canvas() {
  ctx.clearRect(0, 0, canvas_width, canvas_height);
}

function save() {
  dataURL = canvas.toDataURL();
}

function load() {
  var imageObj = new Image();
  imageObj.onload = function () {
    ctx.clearRect(0, 0, canvas_width, canvas_height);
    ctx.drawImage(this, 0, 0);
  };
  imageObj.src = dataURL;
}